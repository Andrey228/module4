using System;

namespace M4
{
    public class Module4
    {
       
        static void Main(string[] args)
        {
            Console.WriteLine("Input");
            int a = int.Parse(Console.ReadLine());
            Console.ReadKey();
        }


        public int Task_1_A(int[] array)
        {
            int max = array[0];
            for (int i=1;i<array.Length;i++)  
                if(array[i]>max)
                    max = array[i];
            return max;
        }

        public int Task_1_B(int[] array)
        {
            int min = array[0];
            for (int i = 1; i < array.Length; i++)
                if (array[i] < min)
                    min = array[i];
            return min;
        }

        public int Task_1_C(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
                sum += array[i];
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            int max = Task_1_A(array);
            int min = Task_1_B(array);
            return max-min;
        }

        public void Task_1_E(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                    array[i] += Task_1_A(array);
                else
                    array[i] -= Task_1_D(array);
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a+b+c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int c;

            if (a.Length >= b.Length)
                c = a.Length;
            else
                c = b.Length;
            int[] array = new int[c];
            if (a.Length >= b.Length)
                a.CopyTo(array, 0);
            else
                b.CopyTo(array, 0);
            for (int i=0;i<a.Length+b.Length-c;i++)
            {
                if (c == a.Length)
                    array[i] += b[i];
                else
                    array[i] += a[i];
            }
            return array;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = Task_1_A(array);
            minItem = Task_1_B(array);
            sumOfItems = Task_1_C(array);
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
           return  (2 * Math.PI * radius, Math.PI * Math.Pow(radius, 2));
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            return (Task_1_A(array), Task_1_B(array), Task_1_C(array));
        }

        public void Task_5(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
                array[i] += 5;
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            int tmp;
            for(int i=0;i<array.Length-1;i++)
            { 
                if (direction == SortDirection.Ascending)
                    if (array[i] > array[i + 1])
                    {
                        tmp = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = tmp;
                    }
                if (direction == SortDirection.Descending)
                    if (array[i] < array[i + 1])
                    {
                        tmp = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = tmp;
                    }
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (Math.Abs(x1 - x2) / Math.Abs(x2) > e)
            {
                result = (x1 + x2) / 2;
                if (func(x1) * func(result) < 0)
                    x2 = result;
                else
                    x1 = result;
                return Task_7(func, x1, x2, e);
            }
            else
                return (x1 + x2) / 2;
        }
    }
}